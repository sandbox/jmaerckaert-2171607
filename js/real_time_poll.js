/**
 * @file 
 * real_time_poll.js
 *
 * Node JS Callbacks and general Javascript code 
 * for the Node JS Real Time Poll module.
 */

(function ($) {
Drupal.theme.prototype.displayBar = function(percentage) {
  return '<div style="width: ' + percentage + '%;" class="foreground"></div>';
}

Drupal.theme.prototype.displayPercent = function(percentage) {
  return '<div class="percent">' + percentage + '%</div>';
}

Drupal.Nodejs.callbacks.real_time_vote = {
    callback: function (message) {
      if(message.callback == 'real_time_vote') {
        var results = Drupal.settings.real_time_poll.poll_results;
        Drupal.settings.real_time_poll.total_votes++;
        $('#block-real-time-poll-real-time-poll .total').html(Drupal.t('Total votes: ') + Drupal.settings.real_time_poll.total_votes);
        var results = Drupal.settings.real_time_poll.results;
        results = results.split(',');

        $('#block-real-time-poll-real-time-poll .choice')
        .each(function(i, bar){
          if(i == (message.data.choice - 1)) {
            results[i]++;
          }
          var percentage = Math.round(results[i] * 100 / Drupal.settings.real_time_poll.total_votes);
          $(this).find(".percent").html(percentage + '%');
          $(this).find(".bar").html(Drupal.theme('displayBar', percentage));
        });
        Drupal.settings.real_time_poll.results = results.join(',');
      }
    }
};

Drupal.Nodejs.callbacks.real_time_cancel_vote = {
    callback: function (message) {
      if(message.callback == 'real_time_cancel_vote') {
        var results = Drupal.settings.real_time_poll.poll_results;
        Drupal.settings.real_time_poll.total_votes--;
        $('#block-real-time-poll-real-time-poll .total').
          html(Drupal.t('Total votes: ') +
              Drupal.settings.real_time_poll.total_votes);
        var results = Drupal.settings.real_time_poll.results;
        results = results.split(',');

        $('#block-real-time-poll-real-time-poll .choice').each(function(i, bar){
          if(i == (message.data.choice - 1)){
            results[i]--;
          }
          var percentage = Math.round(results[i] * 100 / Drupal.settings.real_time_poll.total_votes);
          $(this).find(".percent").html(percentage + '%');
          $(this).find(".bar").html(Drupal.theme('displayBar', percentage));
        });
        Drupal.settings.real_time_poll.results = results.join(',');
      }
    }
};
})(jQuery);
