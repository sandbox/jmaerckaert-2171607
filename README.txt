Realtime Poll
=============

This module adds Node.js integration to Drupal.

CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * INSTALLATION
 * USAGE


INTRODUCTION
------------

Current Maintainer: Jonathan Maerckaert <jmaerckaert@gmail.com>

The Real time poll module provides a result poll block that allows the users of 
a site to see realtime results of the last poll that relies on the Node JS
server provided by the Node.js module <http://drupal.org/project/nodejs>.

INSTALLATION
------------

To install the Realtime Poll:

 1. Place its entire folder into the "sites/all/modules/contrib" folder of your
    drupal installation.

 2. In your Drupal site, navigate to "admin/modules", search the "Realtime poll"
    module, and enable it by clicking on the checkbox located next to it.

 3. Click on "Save configuration".

 4. Enjoy.

USAGE
-----

After installing the module:

  1. Navigate to "admin/structure/block", and place the "Realtime poll Block"
     block in a theme region of your choice.

  2. Alter any of the visibility settings as you would usually do with any other
     block. The are no actual permissions to set up in order to let users use
     the module, although it won't be displayed for anonymous users in its
     current state. This is likely to change in the future.
